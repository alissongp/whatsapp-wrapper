from django.core.exceptions import ValidationError
from django.conf import settings

class ConnectValidation(object):
    def __init__(self, headers, body):
        validation_errors = []

        if settings.PROVIDER_KEY not in headers:
            validation_errors.append(ValidationError(_(f'Missing header: [{settings.PROVIDER_KEY}]'), code='missed'))
        if settings.WAREHOUSE_KEY not in headers:
            validation_errors.append(ValidationError(_(f'Missing header: [{settings.WAREHOUSE_KEY}]'), code='missed'))
        if "userId" not in body:
            validation_errors.append(ValidationError(_('Required value: [userId]'), code='required'))
        elif type(body["userId"]) != str:
            validation_errors.append(ValidationError(_('Invalid value: [userId]'), code='invalid'))
        if "broadcastId" not in body:
            validation_errors.append(ValidationError(_('Required value: [broadcastId]'), code='required'))
        elif type(body["broadcastId"]) != str:
            validation_errors.append(ValidationError(_('Invalid value: [broadcastId]'), code='invalid'))

        if validation_errors:
            raise ValidationError(validation_errors)