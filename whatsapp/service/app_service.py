import json
import logging
import urllib3

from django.conf import settings

log = logging.getLogger(__name__)

def notify_server(data, provider):
    encoded_data = json.dumps(data.__dict__).encode('utf-8')
    url = settings.SERVER_API + '/api/finish-broadcast'
    
    http = urllib3.PoolManager()
    r = http.request(
        'POST',
        url,
        body=encoded_data,
        headers={'Content-Type': 'application/json', settings.PROVIDER_KEY: provider}
    )

    log.info(f'Tryied to notify server, response: {r.data}')
