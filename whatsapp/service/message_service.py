import time
import logging
import threading
import django.apps
import random

from whatsapp import models
from whatsapp.rabbitmq import Rabbit
from django.conf import settings

log = logging.getLogger(__name__)
user = dict()

def connect_user(body, provider, warehouse):
    if body["userId"] in user:
        return settings.USER_ALREADY_CONNECTED
    else:
        user[body["userId"]] = models.User(body["userId"], body["broadcastId"], provider, warehouse)
        return user[body["userId"]].qr_code

def verify(user_id):
    if user_id in user:
        try:
            return user[user_id].verify_and_continue()
        except:
            log.exception(f'Error on User {user_id} scanning QR')
            try:
                user[user_id].destroy()
            except:
                pass
    return False

def delete_user(user_id):
    if user_id in user:
        log.info(f'Removing User [{user_id}] from stack')
        del user[user_id]
