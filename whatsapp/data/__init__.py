class Message(object):
    def __init__(self, data):
        self.id = data["id"]
        self.phone = data["phone"]
        self.content = data["message"]
        self.status = False

class BroadcastResponse(object):
    def __init__(self, user_id, broadcast_id, items):
        self.userId = user_id
        self.broadcastId = broadcast_id
        self.listItems = items