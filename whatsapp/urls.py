from django.urls import path, re_path
from . import views

urlpatterns = [
    path('connect/', views.connect, name='connect'),
    re_path(r'^verify/(?P<user_id>[\w-]{0,50})/$', views.verify)
]
