import logging

from django.apps import AppConfig

log = logging.getLogger(__name__)

class WhatsappConfig(AppConfig):
    name = 'whatsapp'
