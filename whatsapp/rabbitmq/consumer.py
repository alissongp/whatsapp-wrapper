import atexit
import json
import logging
import multiprocessing
import queue
import sys
import threading
from functools import partial
from typing import Tuple, List, Iterator

from pika import ConnectionParameters, SelectConnection
from pika.channel import Channel
from pika.spec import Basic, BasicProperties

log = logging.getLogger(__name__)


class Stoppable(threading.Thread):
    def stop(self):
        sys.exit()

class RabbitConsumer(object):
    def __init__(self, queue_name: str, parameters: ConnectionParameters):
        self.parameters = parameters
        self.queue_name = queue_name

        self.pool = multiprocessing.Pool(processes=2)
        self.manager = multiprocessing.Manager()
        self.buff = self.manager.Queue()

        self.chan = None
        self._closing = False
        self._conn = self.connect()
        self.th = Stoppable(target=self._start)
        self.th.start()

        atexit.register(self.close)
        log.info('Finished Rabbit consumer configuration')

    def _start(self):
        try:
            log.info(f'Creating rabbit queue {self.queue_name}')
            self._conn.ioloop.start()
        except Exception:
            log.exception('Failed connecting consumer')
        finally:
            log.info('Stopped AMQP IOLoop')

    def connect(self):
        return SelectConnection(parameters=self.parameters, on_open_callback=self._on_connection_open)

    def start_consume(self):
        try:
            self.chan.basic_consume(queue=self.queue_name, on_message_callback=self._handle_receive)
        except Exception:
            log.exception(f'Failed setting channel for consume {self.queue_name}')

    def _on_connection_open(self, _connection: SelectConnection):
        log.info(f'Opened connection for consumer {self.queue_name}')
        self._conn.add_on_close_callback(self._on_connection_close)
        self._conn.channel(on_open_callback=self._on_channel_open)

    def _on_connection_close(self):
        self.chan = None
        if self._closing:
            self._conn.ioloop.stop()

    def _on_channel_open(self, channel: Channel):
        log.info('Rabbit consumer channel opened')
        try:
            self.chan = channel
            self.chan.add_on_close_callback(self._on_channel_close)
            # self.chan.queue_purge(self.queue_name)
            self.chan.queue_declare(self.queue_name, durable=True)
            self.chan.basic_consume(queue=self.queue_name, on_message_callback=self._handle_receive)
        except Exception:
            log.exception('Failed setting channel')

    def _on_channel_close(self, channel, reply_code, reply_text):
        log.warning('Channel %i was closed: (%s) %s', channel, reply_code, reply_text)
        self._conn.close()

    def _handle_receive(self, _channel: Channel, method: Basic.Deliver, _header: BasicProperties, body):
        try:
            log.info(f'Received message {body} from {self.queue_name} with tag [{method.delivery_tag}]')
            data = json.loads(body.decode("utf-8"))
            self.buff.put((data, method.delivery_tag))
        except Exception:
            log.exception(f'Failed parsing body from {self.queue_name}')

    def close(self):
        """
        Closes the connection. If messages did not sent the ACK back
        before the disconnection, they will be considered undelivered.
        """
        log.info(f'Rabbit consumer starting {self.queue_name} shutdown')
        self._closing = True
        if self.chan is not None:
            self.chan.queue_delete(queue=self.queue_name)
            self.chan.close()
        self._conn.ioloop.stop()
        self.pool.terminate()

        self.th.join()
        atexit.unregister(self.close)

        log.info(f'Rabbit consumer {self.queue_name} closed')

    def consume_one(self, timeout=None) -> Tuple[object, int]:
        """
        Consume one single message from the buffer queue. This method
        is not responsible for ACK the message, this is up to the caller.

        :param timeout: timeout for waiting message retrieval.
        :return: a tuple containing the message and the ack_tag
        :raise: exception thrown on timeout
        """
        return self.buff.get(block=True, timeout=timeout)

    def read_batch(self, max_batch_size: int) -> Tuple[List[object], List[int]]:
        """
        Create a batch containing up to `max_batch_size` items. This will block
        until exists at least one message. After consuming the message, the caller
        should ACK the message using the available ack_tag.

        :param max_batch_size: maximum number of messages on the batch.
        :return: a tuple containing the messages and ack_tags.
        """
        collected_messages = [self.buff.get(block=True)]
        for item in range(max_batch_size - 1):
            try:
                collected_messages.append(self.buff.get(block=False))
            except queue.Empty:
                break

        msgs = [m[0] for m in collected_messages]
        tags = [t[1] for t in collected_messages]
        return msgs, tags

    def consume(self, max_batch_size: int) -> Iterator[Tuple[List[object], List[int]]]:
        while True:
            yield self.read_batch(max(max_batch_size, 1))

    def ack(self, tags: List[int]):
        if self.chan is None:
            log.warning(f'Cannot ack {tags}')
            return
        if isinstance(tags, list):
            for tag in tags:
                self._conn.ioloop.add_callback_threadsafe(partial(self.chan.basic_ack, delivery_tag=tag))
        else:
            self.chan.basic_ack(tags)
