import threading
import logging
import random
import time
import pika

from pika.exceptions import AMQPConnectionError
from django.conf import settings

from .consumer import RabbitConsumer
from ..service.app_service import notify_server
from ..data import BroadcastResponse 
from whatsapp import models

log = logging.getLogger(__name__)

class Rabbit(object):

    def __init__(self, queue: str, user):
        self.queue = queue
        self.user = user
        self.callback = user.send_message
        self.parameters = pika.ConnectionParameters(host=settings.RABBITMQ_HOST,
                                                    port=settings.RABBITMQ_PORT,
                                                    credentials=pika.PlainCredentials(settings.RABBITMQ_USER, settings.RABBITMQ_PASS),
                                                    heartbeat=30)
        self.delay_interval = list(map(int, settings.TTL_DELAY_INTERVAL.split(',')))
        self.stopped = False
        self.messages = []

    def connect_consumer(self):
        th = threading.Thread(target=self._consumer)
        th.start()

    def _consumer(self):
        while True:
            if self.stopped:
                break
            log.info("Starting Rabbit consumer")
            try:
                consumer = RabbitConsumer(self.queue, self.parameters)
                for (batch, tags) in consumer.consume(100):
                    assert len(batch) == len(tags)
                    for i in range(len(batch)):
                        self._consume_message(consumer, batch[i], tags[i])
            except AMQPConnectionError:
                log.exception("Failed connecting consumer")
            except EOFError: # ignored
                pass
            except Exception:
                log.exception("Unknown error connecting consumer")

    def _consume_message(self, consumer: RabbitConsumer, msg: object, tag: int):
        try:
            delay = random.uniform(self.delay_interval[0], self.delay_interval[1])

            if type(msg) == str and msg == settings.DISCONNECT_KEY:
                notify_server(BroadcastResponse(self.user.id, self.user.broadcast_id, self.messages), self.user.provider)
                consumer.close()
                self.stopped = True
                self.user.destroy()
            else:
                log.info(f'Sending message [{msg["id"]}-{tag}] in {delay:.2f} seconds')
                time.sleep(delay)
                self.messages.append(self.callback(msg))
        except Exception:
            log.exception("Failed processing {}", msg)
        finally:
            consumer.ack([tag])
