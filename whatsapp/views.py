import json
import logging

from django.http import HttpResponse, HttpResponseNotFound, HttpResponseServerError
from .service import message_service as service
from .validators import ConnectValidation
from django.conf import settings

log = logging.getLogger(__name__)

def connect(request):
    if request.method == 'POST':
        try:
            body = json.loads(request.body.decode("utf-8"))
            headers = request.headers
            ConnectValidation(headers, body)

            qr_code = service.connect_user(body, headers[settings.PROVIDER_KEY], headers[settings.WAREHOUSE_KEY])
            return HttpResponse(json.dumps(qr_code), content_type='text/plain')
        except Exception:
            log.exception("Server error during connection")
            return HttpResponseServerError()
    else:
        return HttpResponseNotFound()

def verify(request, user_id):
    if request.method == 'GET':
        try:
            return HttpResponse(json.dumps(service.verify(user_id)), content_type='application/json')
        except Exception:
            log.exception("Server error during scan")
            return HttpResponseServerError()
    else:
        return HttpResponseNotFound()
