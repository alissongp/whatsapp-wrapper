import os
import logging

from webwhatsapi import WhatsAPIDriver
from .service.message_service import delete_user
from django.db import models
from django.conf import settings
from .rabbitmq import Rabbit
from .data import Message

log = logging.getLogger(__name__)

class User(models.Model):

    class Meta:
        managed = False

    def __init__(self, id, broadcast_id, provider, warehouse):
        self.id = id
        self.broadcast_id = broadcast_id
        self.provider = provider
        self.warehouse = warehouse

        self.driver = self.init_driver()
        self.qr_code = self.driver.get_qr_base64()
        self.connected = False

        self.rabbit_queue_name = f'{self.provider}-{self.warehouse}-{self.id}'
        self.rabbit_queue = Rabbit(self.rabbit_queue_name, self)

    def init_driver(self):
        log.info("Initializing Driver for {}".format(self))

        return WhatsAPIDriver(
            loadstyles=True,
            client='firefox',
            headless=True
        )
    
    def verify_and_continue(self):
        self.driver.wait_for_login(settings.TIMEOUT_CODE_EXPIRATION)
        self.connected = True
        self.rabbit_queue.connect_consumer()

        return True
    
    def send_to_wpp(self, data: Message):
        self.driver.create_if_not_exists(self.format_phone(data["phone"]))
        self.driver.chat_send_message(self.format_phone(data["phone"]), data["message"])

        data["status"] = True
        log.info(f'Success on send message to [{data["phone"]}]')

    def send_message(self, data: Message):
        try:
            self.send_to_wpp(data)
        except:
            try:
                data["phone"] = self.format_nine(data["phone"])
                self.send_to_wpp(data)
            except Exception:
                data["status"] = False
                log.exception(f'Error on send message to [{data["phone"]}]')

        return data
        
    def format_nine(self, phone):
        if len(phone.split("@")[0]) == 10:
            return phone[0:2] + "9" + phone[2:]
        elif len(phone.split("@")[0]) == 11:
            return phone[0:2] + phone[3:]
        return phone

    def format_phone(self, phone):
        if not phone.startswith('55'):
            phone = '55' + phone
        if not phone.endswith('@c.us'):
            phone = phone + '@c.us'

        return phone

    def destroy(self):
        self.connected = False
        try:
            log.info(f'Closing driver for {self}')
            self.driver.close()
        except Exception:
            log.warn(f'Something unexpected happened on closing driver for {self}')
        
        delete_user(self.id)

    def __str__(self):
        return f'User[id: [{self.id}], broadcast_id: [{self.broadcast_id}]]'
