# Whatsappweb Wrapper #

### Environment ###

Necessário para funcionamento

    python3.6
    pip
    firefox > testado na 59.0.2
    geckodriver
    virtualenv

### Como executar ###

Em desenvolvimento recomande-se usar virtualenv, criado com:

    virtualenv -p python3.6 .

Instalar as dependências com:

    pip install -r requirements.txt

Com o ambiente virtual ativado:```source bin/activate```, rode com as configurações locais:

    python start.py dev
    
Ou utilize as properties do Consul:

    python start.py HOSTNAME=<VALUE> TOKEN=<VALUE> REPO=<VALUE>

### Configurações básicas para desenvolvimento ###

    {
        "rabbitHost": "rabbitmq",
        "rabbitPort": "5672",
        "rabbitUser": "tm2digital",
        "rabbitPass": "n3pp02017",
        "gatewayURL": "http://localhost:5555",
        "chatURL": "http://localhost:8080/chat",
        "webDavHost": "https://rapid01-storage.tm2digital.com/storage",
        "webDavToken": "d93def44-c50f-4166-8d44-f06781fbd966"
     }

### Note ###

* Esse projeto funciona em conjunto com [Whatsappweb-gateway-node](https://bitbucket.org/neppo/whatsappweb-gateway-node/src/master/), logo é necessário rodar os dois.
* Sempre rode o Gateway Node primeiro.

### Limpar conversas antigas ###

Por padrão as mensagens serão limpas a cada 2 horas. Para alterar este tempo utilize a variável de ambiente CLEAN_RATE seguindo o padrão:

. Por horas: 1H, 3H, 12H
. Por dias: 1D, 3D, 30D
